
import styles from "./MainLayout.module.css"

import { Navbar } from "../Navbar"
import Head from "next/head"

export const MainLayout = ({ children }) => {
    return (
        <div className={styles.container}>
          <Head>
            <title>Index Home- Agetic</title>
            <meta name="description" content="Generated by create next app" />
            <link rel="icon" href="/favicon.ico" />
          </Head>
          <Navbar/>
          <main className={styles.main}>

            {children}
    
          </main>
    
        </div>
      )
}
