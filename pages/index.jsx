import Link from 'next/link'
import { MainLayout } from '../components/layouts/MainLayout.jsx'


export default function HomeMundo() {
  return (
    <MainLayout>
            Home Page<br></br>
        <h1 className={'title'}>
          {/* Desde home ir a  <a href="about">About!</a> */}
          
          Ir a <Link href="about" replace>About</Link>
          Ir a <Link href="contact" replace>Contact</Link>
        </h1>

        <p className={'description'}>
          Get started by editing
          <code className={'code'}>pages/index.js</code>
        </p>

    </MainLayout>
  )
}
