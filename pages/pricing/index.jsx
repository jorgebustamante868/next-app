import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { MainLayout } from '../../components/layouts/MainLayout'
import { Navbar } from '../../components/Navbar'
// import styles from '../../styles/Home.module.css'

export default function HomeMundo() {
  return (
   
    <MainLayout>
        <h1 className={'title'}>
          {/* Desde home ir a  <a href="about">About!</a> */}

          Ir a <Link href="/" replace>Home</Link>
          Ir a <Link href="about" replace>About</Link>
          Ir a <Link href="contact" replace>Contact</Link>
        </h1>

        <p className={'description'}>
          Get started by editing{' '}
          <code className={'code'}>pages/index.js</code>
        </p>
    </MainLayout>
  )
}
